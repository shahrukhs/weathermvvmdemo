//
//  FirstScreenViewModel.swift
//  MVVMPractice
//
//  Created by webwerks on 20/05/19.
//  Copyright © 2019 webwerks. All rights reserved.
//

import Foundation
import UIKit

class FirstScreenViewModel {
    
    var item : FirstViewData?
    var cell: FirstScreenTableViewCell?
    
    public func updateCellValue(cell: FirstScreenTableViewCell, item: FirstViewData) {
        if item.count! == 0 {
            cell.minusButton.isEnabled = false
        }
        
        cell.titleLabel.text = item.title
        cell.countLabel.text = "\(String(describing: item.count!))"
    }
    
    public func plusButtonClick(itemTemp: FirstViewData, completion: @escaping ( _ result: FirstViewData) -> Void) {
//        item = itemTemp
        let count = item!.count! + 1
        item!.count = count
        
        cell!.minusButton.isEnabled = true
        
        cell!.titleLabel.text = item!.title
        cell!.countLabel.text = "\(String(describing: item!.count!))"
        
        completion(item!)
    }
    
    public func minusButtonClick(itemTemp: FirstViewData, completion: @escaping ( _ result: FirstViewData) -> Void) {
//        item = itemTemp
        if item!.count! > 0 {
            let count = item!.count! - 1
            item!.count = count
            
            cell!.titleLabel.text = item!.title
            cell!.countLabel.text = "\(String(describing: item!.count!))"
            
            completion(item!)
        } else {
            cell!.minusButton.isEnabled = false
        }
    }
    
}
