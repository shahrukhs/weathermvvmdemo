//
//  SecondScreenVC.swift
//  MVVMPractice
//
//  Created by webwerks on 23/05/19.
//  Copyright © 2019 webwerks. All rights reserved.
//

import UIKit

class SecondScreenVC: UIViewController {

    let navigationView = NavigationView()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationController?.hidesBarsOnSwipe = false
        
//        let myView = Bundle.main.loadNibNamed("NavigationView", owner: self, options: nil)!.first as! NavigationView
        
        navigationView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 44)
        
//        navigationView.contentView.frame = myView.bounds
        
//        navigationView.translatesAutoresizingMaskIntoConstraints = false
        
        self.navigationItem.titleView = navigationView
        
        self.navigationView.backButtonClosuer = {
            self.navigationController?.popViewController(animated: true)
        }
        
        self.navigationItem.setHidesBackButton(true, animated: true)
        
        let myView = NavigationView()
        myView.frame = CGRect(x: 0, y: 100, width: self.view.frame.size.width, height: 100)
        
        self.view.addSubview(myView)
        
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
