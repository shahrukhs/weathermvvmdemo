//
//  FirstScreenTableViewCell.swift
//  MVVMPractice
//
//  Created by webwerks on 20/05/19.
//  Copyright © 2019 webwerks. All rights reserved.
//

import UIKit

class FirstScreenTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var countLabel: UILabel!
    
    @IBOutlet weak var minusButton: UIButton!
    @IBOutlet weak var plusButton: UIButton!
    
    var plusButtonClosure: ((Int) -> Void)?
    var minusButtonClosure: ((Int) -> Void)?
    
    var indexPath: IndexPath?
    
    var item: FirstViewData! {
        didSet {
            if item.count == 0 {
                minusButton.isEnabled = true
            }
            
            titleLabel.text = item.title
            countLabel.text = "\(String(describing: item.count!))"
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func minusAction(_ sender: UIButton) {
        if let clickAction = minusButtonClosure {
            clickAction(indexPath!.row)
        } else {
            assert(false, "warn click closure is nil")
        }
    }
    
    @IBAction func plusAction(_ sender: UIButton) {
        if let clickAction = plusButtonClosure {
            clickAction(indexPath!.row)
        } else {
            assert(false, "warn click closure is nil")
        }
    }
    
}
