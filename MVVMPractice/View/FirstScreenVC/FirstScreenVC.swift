//
//  FirstScreenVC.swift
//  MVVMPractice
//
//  Created by webwerks on 20/05/19.
//  Copyright © 2019 webwerks. All rights reserved.
//

import UIKit

class FirstScreenVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var model : FirstViewModel?
    
    var viewModel = FirstScreenViewModel()
    
    var totalHeaderHeight: CGFloat = 40.0
    
//    lazy var profileHeaderView: ProfileHeaderView = {
//        let headerView = ProfileHeaderView()
//        return headerView
//    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let data = ["data": [["title": "title 1", "count": 0], ["title": "title 2", "count": 0], ["title": "title 3", "count": 0], ["title": "title 4", "count": 0]]]
        
        let jsonObj = try! JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
        
        model = try! JSONDecoder().decode(FirstViewModel.self, from: jsonObj)
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        navigationController?.hidesBarsOnSwipe = true
//        self.navigationController?.navigationBar
        
        print(model?.data?[0].title ?? "Default value")
        
        tableView.dataSource = self
        tableView.delegate = self
        tableView.tableFooterView = UIView()
        
//        tableView.tableHeaderView = getHeaderView()
        
        self.tableView.sectionHeaderHeight = UITableView.automaticDimension;
        
        self.tableView.estimatedSectionHeaderHeight = 38
        
        setNeedsStatusBarAppearanceUpdate()
        
        tableView.contentInset = UIEdgeInsets(top: -90, left: 0, bottom: 0, right: 0)
        tableView.contentInsetAdjustmentBehavior = .never
    }

    func plusAction(index: Int) {
        viewModel.item = (model?.data?[index])!
        viewModel.plusButtonClick(itemTemp: (model?.data?[index])!, completion: { item in
            self.model?.data?[index] = item
            self.tableView.reloadData()
        })
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    func minusAction(index: Int) {
        viewModel.item = (model?.data?[index])!
        viewModel.minusButtonClick(itemTemp: (model?.data?[index])!, completion: { item in
            self.model?.data?[index] = item
            self.tableView.reloadData()
        })
    }
    
    func getHeaderView() -> UIView {
        let headerView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: self.tableView.frame.size.width, height: 10))
        
        headerView.backgroundColor = .black
        
        let label = UILabel(frame: CGRect(x: 10, y: 5, width: self.tableView.frame.size.width - 10, height: 21))
        label.text = "self.listSpeakerData.name"
        //                    label.font = UIFont(name: "robotocondensed-bold", size: 13)
        label.textColor = .black
        label.numberOfLines = 1
        label.sizeToFit()
        
        let label2 = UILabel(frame: CGRect(x: 10, y: label.frame.origin.y + label.frame.height + 5, width: self.tableView.frame.size.width - 10, height: 21))
        label2.text = "\n" + "self.hrdData"
        //  label2.text = "self.hrdData"
        //                    label2.font = UIFont(name: "roboto-regular", size: 13)
        label2.textColor = .black
        label2.numberOfLines = 0
        label2.sizeToFit()
        
        //draw a line.
        let sepFrame1 = CGRect(x:0, y: label2.frame.origin.y + label2.frame.height,
                               width: self.tableView.frame.size.width, height: 1)
        let seperatorView = UIView(frame: sepFrame1);
        seperatorView.backgroundColor = UIColor.darkGray
        
        headerView.addSubview(label)
        headerView.addSubview(label2)
        headerView.addSubview(seperatorView);
        
        //            headerView = UIView(frame: CGRect(x: 0, y: 0, width: self.tableView.frame.size.width, height: label.frame.origin.y + label.frame.size.height + label2.frame.origin.y + label2.frame.size.height + seperatorView.frame.origin.y + seperatorView.frame.size.height))
        
        headerView.frame = CGRect(x: 0, y: 0, width: self.tableView.frame.size.width, height:  seperatorView.frame.origin.y + seperatorView.frame.size.height)
        
        return headerView
    }
    
}

extension FirstScreenVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if model != nil {
            return (model?.data!.count)!
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: FirstScreenTableViewCell = tableView.dequeueReusableCell(withIdentifier: "FirstScreenTableViewCell", for: indexPath) as! FirstScreenTableViewCell
        
        cell.item = model?.data?[indexPath.row]
        cell.indexPath = indexPath
        
        viewModel.cell = cell
        cell.plusButtonClosure = plusAction
        cell.minusButtonClosure = minusAction
        
        return cell
    }
}

extension FirstScreenVC: UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SecondScreenVC") as! SecondScreenVC
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
   
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        return totalHeaderHeight
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView: UIView = getHeaderView()
       
        totalHeaderHeight = headerView.frame.size.height
        return headerView
    }

}


