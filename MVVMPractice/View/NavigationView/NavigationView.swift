//
//  NavigationView.swift
//  MVVMPractice
//
//  Created by webwerks on 23/05/19.
//  Copyright © 2019 webwerks. All rights reserved.
//

import UIKit

class NavigationView: UIView {

    let kCONTENT_XIB_NAME = "NavigationView"
    @IBOutlet var contentView: UIView!
    @IBOutlet var view: UIView!
    
    override var intrinsicContentSize: CGSize {
        return UIView.layoutFittingExpandedSize
    }
    
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        self.xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
        self.xibSetup()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    func xibSetup() {
        Bundle.main.loadNibNamed("NavigationView", owner: self, options: nil)
        addSubview(view)
        view.frame = self.bounds
        view.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    }
    
    func loadViewFromNib() -> UIView {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "NavigationView", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        
        return view
    }
    
    var backButtonClosuer: (() -> Void)?
    
    @IBAction func backButtonClickAction(sender: UIButton) {
        if let click = backButtonClosuer {
            click()
        } else {
            assert(false, "back button click closure is nil")
        }
    }
    
}
