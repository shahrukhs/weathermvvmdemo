//
//	FirstViewData.swift
//
//	Create by webwerks on 20/5/2019
//	Copyright © 2019. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

struct FirstViewData : Codable {

	var count : Int?
	var title : String?


	enum CodingKeys: String, CodingKey {
		case count = "count"
		case title = "title"
	}
    
	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		count = try values.decodeIfPresent(Int.self, forKey: .count)
		title = try values.decodeIfPresent(String.self, forKey: .title)
	}


}
