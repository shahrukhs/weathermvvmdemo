//
//	FirstViewModel.swift
//
//	Create by webwerks on 20/5/2019
//	Copyright © 2019. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

struct FirstViewModel : Codable {

	var data : [FirstViewData]?


	enum CodingKeys: String, CodingKey {
		case data = "data"
	}
	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		data = try values.decodeIfPresent([FirstViewData].self, forKey: .data)
	}


}
